import com.tngtech.jgiven.annotation.ScenarioStage;
import com.tngtech.jgiven.junit.JGivenClassRule;
import com.tngtech.jgiven.junit.JGivenMethodRule;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

public class TestRunner {

    @ClassRule
    public static final JGivenClassRule writerRule = new JGivenClassRule();

    @Rule
    public final JGivenMethodRule scenarioRule = new JGivenMethodRule();

    @ScenarioStage
    TestStage testStage;

    @Test
    public void check_app_version() {
        testStage.given().open_settings();
        testStage.when().open_about_screen();
        testStage.then().product_version_is_correct();
    }

    @Test
    public void check_app_languages() {
        testStage.given().open_settings();
        testStage.when().open_languages_list();
        testStage.then().check_languages_list();
    }

    @Test
    public void check_detail_app_window() {
        testStage.given().open_app_activity_list();
        testStage.when().open_app_activity();
        testStage.then().check_app_activity_detail_page();
    }
}
