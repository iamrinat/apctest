import PageObjects.AboutPage;
import PageObjects.AppDetailPage;
import PageObjects.MainPage;
import PageObjects.SettingsPage;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.AfterScenario;
import com.tngtech.jgiven.annotation.BeforeScenario;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;

public class TestStage extends Stage<TestStage> {
    AndroidDriver driver;
    Properties prop = new Properties();
    MainPage mainPage;
    SettingsPage settingsPage;
    AboutPage aboutPage;
    AppDetailPage appDetailPage;

    @BeforeScenario
    public void setup() {
        try (InputStream input = TestStage.class.getClassLoader().getResourceAsStream("config.properties")) {
            if (input == null) {
                System.out.println("Sorry, unable to find config.properties");
                return;
            }
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appActivity", ".app.CreateActivity");
        capabilities.setCapability("appPackage", "com.sika524.android.quickshortcut");
        capabilities.setCapability("app", prop.getProperty("apkFile"));
        URL url = null;
        try {
            url = new URL("http://0.0.0.0:4723/wd/hub");
        } catch (MalformedURLException e) {
            url = null;
        }
        assertThat("appium url not null", url != null);
        if (url == null) return;

        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
        mainPage = new MainPage(driver);
        settingsPage = new SettingsPage(driver);
        aboutPage = new AboutPage(driver);
        appDetailPage = new AppDetailPage(driver);
    }

    @AfterScenario
    public void teardown() {
        driver.quit();
    }

    public void makeScreenShot(String filename) {
        File file  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file, new File("Screenshots\\"+filename+".jpg"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public TestStage open_settings() {
        makeScreenShot("1.MainScreen");
        mainPage.menu.click();
        mainPage.settingsMenu.click();
        makeScreenShot("2.SettingsScreen");
        return self();
    }

    public TestStage open_about_screen() {
        settingsPage.scrollDownTo(settingsPage.about);
        settingsPage.about.click();
        makeScreenShot("3.AboutScreen");
        return self();
    }

    public TestStage product_version_is_correct() {
        aboutPage.version.isDisplayed();
        assertThat("Version is incorrect",aboutPage.version.getText().compareTo("2.4.0")==0);
        return self();
    }

    public TestStage open_languages_list() {
        settingsPage.language.click();
        return self();
    }

    public TestStage check_languages_list() {
        //better to load it from external storage
        List<String> languages = new ArrayList<String>() ;
        languages.add("Default");
        languages.add("English");
        languages.add("Italiano");

        languages.forEach(s -> {
            assertThat("Language "+s+" not found",settingsPage.hasLanguage(s));
        });
        return self();
    }


    public TestStage open_app_activity_list() {
        mainPage.appPackage.click();
        return self();
    }
    public TestStage open_app_activity() {
        mainPage.appActivity.click();
        return self();
    }

    public TestStage check_app_activity_detail_page() {
        assertThat("Try activity button not found",appDetailPage.openActivityButton.isDisplayed());
        return self();
    }
}
