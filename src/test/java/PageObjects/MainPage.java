package PageObjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class MainPage extends BasePageObject {
    public MainPage(AndroidDriver driver) {
        super(driver);
    }

    @AndroidFindBy(className = "android.widget.ImageButton")
    public WebElement menu;
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Settings\")" )
    public WebElement settingsMenu;

    @AndroidFindBy(id = "com.sika524.android.quickshortcut:id/ActivityDetail" )
    public WebElement appPackage;

    @AndroidFindBy(id = "com.sika524.android.quickshortcut:id/ActivityLabel" )
    public WebElement appActivity;
}
