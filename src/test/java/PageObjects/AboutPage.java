package PageObjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class AboutPage extends BasePageObject {
    public AboutPage(AndroidDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.sika524.android.quickshortcut:id/version_name" )
    public WebElement version;
}
