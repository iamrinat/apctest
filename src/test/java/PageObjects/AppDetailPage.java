package PageObjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class AppDetailPage extends BasePageObject {
    public AppDetailPage(AndroidDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.sika524.android.quickshortcut:id/icon_try" )
    public WebElement openActivityButton;
}
