package PageObjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SettingsPage extends BasePageObject {
    public SettingsPage(AndroidDriver driver) {
        super(driver);
    }

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Language\")" )
    public WebElement language;

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"About this app\")" )
    public WebElement about;

    public boolean hasLanguage(String language) {
        return this.driver.findElements(By.id("android:id/text1")).stream().anyMatch(webElement -> webElement.getText().compareTo(language)==0);
    }
}
