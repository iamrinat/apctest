package PageObjects;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BasePageObject {
    AndroidDriver driver;

    public BasePageObject(AndroidDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void scrollDownTo(WebElement elem) {
        String current = this.driver.getPageSource();
        this.scrollDown();
        while (current.compareTo(this.driver.getPageSource())!=0) {
            this.scrollDown();
            if (elem.isDisplayed()) {
                break;
            }
        }
    }

    private void scrollDown() {
        int pressX = driver.manage().window().getSize().width / 2;
        int bottomY = driver.manage().window().getSize().height * 4 / 5;
        int topY = driver.manage().window().getSize().height / 8;
        scroll(pressX, bottomY, pressX, topY);
    }

    private void scroll(int fromX, int fromY, int toX, int toY) {
        TouchAction touchAction = new TouchAction((AndroidDriver) this.driver);
        touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
    }
}